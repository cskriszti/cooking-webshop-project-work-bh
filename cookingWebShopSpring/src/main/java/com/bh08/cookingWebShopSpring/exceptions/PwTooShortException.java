package com.bh08.cookingWebShopSpring.exceptions;


public class PwTooShortException extends Exception {

	private static final long serialVersionUID = 1L;

	public PwTooShortException() {
		super("Password too short,Please enter at least 4 characters." );
	}}
	