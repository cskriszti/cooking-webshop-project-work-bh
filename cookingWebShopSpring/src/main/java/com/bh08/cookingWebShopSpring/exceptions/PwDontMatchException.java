package com.bh08.cookingWebShopSpring.exceptions;

public class PwDontMatchException extends Exception {

	private static final long serialVersionUID = 1L;

	public PwDontMatchException() {
		super("Password and confirm password don't match.");
	}

}
