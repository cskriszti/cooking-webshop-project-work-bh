package com.bh08.cookingWebShopSpring.services;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.bh08.cookingWebShopSpring.daos.MealDbDao;
import com.bh08.cookingWebShopSpring.daos.RecipeDao;
import com.bh08.cookingWebShopSpring.daos.UserRepository;
import com.bh08.cookingWebShopSpring.models.Product;
import com.bh08.cookingWebShopSpring.models.Recipe;
import com.bh08.cookingWebShopSpring.models.User;
import com.bh08.cookingWebShopSpring.models.integration.mealdb.MealDbIngredientListDto;
import com.bh08.cookingWebShopSpring.models.integration.mealdb.MealDbMealDto;

@Service
public class RecipeServices {
	
	@Autowired
	MealDbDao dao;
	
	@Autowired
	RecipeDao recipeDao;

	@Autowired
	UserRepository userDao;
	
	private static final int PAGE_SIZE = 4;
	
	public void saveIngredients(MealDbIngredientListDto listOfIngredients) {
		Random random = new Random();
		for (int i = 0; i < listOfIngredients.getMeals().size(); i++) {
			int randomPrice = random.nextInt(2000 - 100) + 100;
			dao.saveAndFlush(
					new Product(listOfIngredients.getMeals().get(i).getStrIngredient().toLowerCase(), randomPrice));
		}
	}
	
	public List<Recipe> createRecipes(List<MealDbMealDto> finalMealList) {

		List<Recipe> recipes = new ArrayList<Recipe>();
		
		for (int recipeId = 0; recipeId < finalMealList.size(); recipeId++) {
			Recipe recipe = new Recipe();
			recipe.setId(Long.parseLong(finalMealList.get(recipeId).getIdMeal()));
			recipe.setName(finalMealList.get(recipeId).getStrMeal());
			recipe.setMealPictureLink(finalMealList.get(recipeId).getStrMealThumb());
			recipe.setMealYuotubeLink(finalMealList.get(recipeId).getStrYoutube());
			recipe.setStrInstructions(finalMealList.get(recipeId).getStrInstructions());

			Set<Product> productList = new HashSet<>();

			for (int ingredientId = 1; ingredientId <= 20; ingredientId++) {

				MealDbMealDto recipeDto = finalMealList.get(recipeId);
				Field f;
				String ingredientName = null;
				try {

					f = recipeDto.getClass().getDeclaredField("strIngredient" + ingredientId);
					f.setAccessible(true);
					ingredientName = (String) f.get(recipeDto);

				} catch (NoSuchFieldException | SecurityException | IllegalArgumentException
						| IllegalAccessException e) {
					e.printStackTrace();
				}

				if (ingredientName != null && !"".equals(ingredientName)) {

					productList.add(dao.findFirstByName(ingredientName.toLowerCase()));

				}

			}

			recipe.setProducts(productList);
			recipes.add(recipe);
		}

		return recipes;
	}

	public void saveRecipeToDb(List<Recipe> recipes) {
		for (Recipe r : recipes) {
			recipeDao.saveAndFlush(r);
		}
	}

	public Page<Recipe> getAllRecipes(Integer pageNumber) {
		PageRequest pageRequest = PageRequest.of(pageNumber, 4);
		Page<Recipe> allRecipes = recipeDao.findAll(pageRequest);
		return allRecipes;
	}

	public Recipe getRecipeDetails(Long recipeId) {
		Recipe recipe = recipeDao.findFirstById(recipeId);

		return recipe;
	}

	public List<Recipe> getRandomRecipes() {
		List<Recipe> randomRecipes;
		List<Long> randomIds = new ArrayList<>();

		for (int i = 0; i < 4; i++) {
			Long recipeId = (long) (52764 + (Math.random()) * (52966 - 52764));
			if (randomIds.contains(recipeId)) {
				i--;
			} else {
				if (recipeDao.existsById(recipeId)) {
					randomIds.add(recipeId);
				} else {
					i--;
				}

			}
		}

		randomRecipes = recipeDao.findByIdIn(randomIds);

		return randomRecipes;
	}

	public Page<Recipe> findByTitleIgnoreCaseContains(Integer pageNr, String name) {
		PageRequest pageRequest = PageRequest.of(pageNr, 4);
		Page<Recipe> searchResult = recipeDao.findByNameIgnoreCaseContains(name, pageRequest);

		return searchResult;
	}

	public void saveOrDeleteById(Long mealId, String userName) {

		Optional<Recipe> recipe = recipeDao.findById(mealId);
		Optional<User> user = userDao.findByUserName(userName);

		Set<Recipe> favRecipes = (user.get()).getFavouriteRecipes();

		if (favRecipes.contains(recipe.get())) {
			favRecipes.remove(recipe.get());

			user.get().setFavouriteRecipes(favRecipes);
			userDao.saveAndFlush(user.get());

		} else {
			favRecipes.add(recipe.get());

			user.get().setFavouriteRecipes(favRecipes);
			userDao.saveAndFlush(user.get());
		}

	}

	public Page<Recipe> findByUserId(Integer pageNr, Long userId) {
		PageRequest pageRequest = PageRequest.of(pageNr, PAGE_SIZE);

		return recipeDao.findByUsersId(userId, pageRequest);
	}

	public Set<Recipe> findByIngredientsIgnoreCaseContains(String ingred1, String ingred2, String ingred3) {

		List<String> ingredients = new ArrayList<>();
		if (ingred1 != null) {
			ingredients.add(ingred1);
		}
		if (ingred2 != null) {
			ingredients.add(ingred2);
		}
		if (ingred3 != null) {
			ingredients.add(ingred3);
		}

		List<Recipe> searchResult = recipeDao.findByProductsNameIn(ingredients);

		Set<Recipe> recipesWithAllIngredients = searchResult.stream().filter(recipe -> {
			boolean allIngredientsFound = true;
			for (String ingrName : ingredients) {
				if (!"".equals(ingrName)) {
					long ingrCountWithThisName = recipe.getProducts().stream().filter(p -> p.getName().equals(ingrName))
							.count();
					boolean ingredientFound = ingrCountWithThisName > 0;
					if (!ingredientFound) {
						allIngredientsFound = false;
						break;
					}
				}
			}
			return allIngredientsFound;
		}).collect(Collectors.toSet());

		return recipesWithAllIngredients;
	}

}
