package com.bh08.cookingWebShopSpring.services;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bh08.cookingWebShopSpring.daos.MealDbDao;
import com.bh08.cookingWebShopSpring.daos.RecipeDao;
import com.bh08.cookingWebShopSpring.daos.UserRepository;
import com.bh08.cookingWebShopSpring.models.integration.mealdb.MealDbIngredientListDto;
import com.bh08.cookingWebShopSpring.models.integration.mealdb.MealDbMealDto;
import com.bh08.cookingWebShopSpring.models.integration.mealdb.MealDbMealListDto;
import com.bh08.cookingWebShopSpring.models.integration.mealdb.MealDbRecipeListDto;

@Service
public class MealDbService {

	private static final String API_ROOT_URL = "https://www.themealdb.com/api/json/v1/1";

	@Autowired
	MealDbDao dao;

	@Autowired
	RecipeDao recipeDao;

	@Autowired
	UserRepository userDao;

	MealDbIngredientListDto ingredientList;
	MealDbMealListDto meals;

	List<MealDbMealDto> finalMealList = new ArrayList<>();

	public MealDbIngredientListDto downloadIngredients() {
		RestTemplate restTemplate = new RestTemplate();
		ingredientList = restTemplate.getForObject(API_ROOT_URL + "/list.php?i=list", MealDbIngredientListDto.class);

		return ingredientList;
	}

	public Set<String> downloadRecipe() {

		RestTemplate restTemplate = new RestTemplate();
		MealDbRecipeListDto recipeList = null;

		Set<String> mealListById = new TreeSet<>();

		for (int i = 0; i < ingredientList.getMeals().size(); i++) {
			recipeList = restTemplate.getForObject(
					API_ROOT_URL + "/filter.php?i=" + ingredientList.getMeals().get(i).getStrIngredient(),
					MealDbRecipeListDto.class);

			if (recipeList.getMeals() != null) {
				for (int j = 0; j < recipeList.getMeals().size(); j++) {
					mealListById.add(recipeList.getMeals().get(j).getIdMeal());
				}
			}
		}

		return mealListById;
	}

	public List<MealDbMealDto> downloadMeals(Set<String> mealsByIdList) {
		RestTemplate restTemplate = new RestTemplate();

		for (String id : mealsByIdList) {
			meals = restTemplate.getForObject(API_ROOT_URL + "/lookup.php?i=" + id, MealDbMealListDto.class);

			finalMealList.add(meals.getMeals().get(0));

		}

		return finalMealList;
	}

	
}
