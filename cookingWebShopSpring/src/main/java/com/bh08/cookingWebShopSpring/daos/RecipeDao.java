﻿package com.bh08.cookingWebShopSpring.daos;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bh08.cookingWebShopSpring.models.Recipe;

@Repository
public interface RecipeDao extends JpaRepository<Recipe, Long> {

	Recipe findFirstById(Long id);

	List<Recipe> findByIdIn(List<Long> randomIds);

	Page<Recipe> findByNameIgnoreCaseContains(String name, Pageable pageRequest);

	Page<Recipe> findByUsersId(Long userId, Pageable pageRequest);

	List<Recipe> findByProductsNameIn(List<String> ingredients);
}
