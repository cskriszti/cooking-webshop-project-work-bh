package com.bh08.cookingWebShopSpring.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bh08.cookingWebShopSpring.models.Product;

@Repository
public interface MealDbDao extends JpaRepository<Product, Long> {
	
	Product findFirstByName(String name);
}
