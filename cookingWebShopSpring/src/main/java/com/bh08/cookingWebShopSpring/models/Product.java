package com.bh08.cookingWebShopSpring.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Entity implementation class for Entity: Product
 *
 */
@Entity
@Table(name = "products")
@Getter
@Setter
@ToString
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	public Product() {
		super();
	}

	@Id
	@GeneratedValue(generator = "productIdGenerator")
	@SequenceGenerator(name = "productIdGenerator", sequenceName = "seqProductId", initialValue = 1)
	private Long id;

	@Column(nullable = false)
	private String name;

	private double price;

	@ManyToMany(mappedBy = "products", fetch = FetchType.EAGER)
	private List<Recipe> recipes;

	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", price=" + price + "]";
	}

}