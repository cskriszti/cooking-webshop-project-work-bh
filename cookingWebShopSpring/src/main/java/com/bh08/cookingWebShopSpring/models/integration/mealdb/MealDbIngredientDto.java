package com.bh08.cookingWebShopSpring.models.integration.mealdb;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MealDbIngredientDto {

	private String idIngredient;
	private String strIngredient;
	
}
