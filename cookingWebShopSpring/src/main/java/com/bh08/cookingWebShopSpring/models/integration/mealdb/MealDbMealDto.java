package com.bh08.cookingWebShopSpring.models.integration.mealdb;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MealDbMealDto {
	
	private String idMeal;
	private String strMeal;
	private String strInstructions;
	private String strMealThumb;
	private String strYoutube; 
	
	private String strIngredient1;
	private String strIngredient2;
	private String strIngredient3;
	private String strIngredient4;
	private String strIngredient5;
	private String strIngredient6;
	private String strIngredient7;
	private String strIngredient8;
	private String strIngredient9;
	private String strIngredient10;
	private String strIngredient11;
	private String strIngredient12;
	private String strIngredient13;
	private String strIngredient14;
	private String strIngredient15;
	private String strIngredient16;
	private String strIngredient17;
	private String strIngredient18;
	private String strIngredient19;
	private String strIngredient20;
	
}
