package com.bh08.cookingWebShopSpring;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EntityScan(basePackages = "com.bh08.cookingWebShopSpring.models")
@ComponentScan(basePackages = "com.bh08.cookingWebShopSpring")
public class SpringConfig {

}
