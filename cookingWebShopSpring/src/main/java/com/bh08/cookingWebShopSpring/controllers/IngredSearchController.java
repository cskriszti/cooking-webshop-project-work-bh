package com.bh08.cookingWebShopSpring.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bh08.cookingWebShopSpring.models.Recipe;
import com.bh08.cookingWebShopSpring.services.RecipeServices;
import com.bh08.cookingWebShopSpring.services.SessionService;
import com.bh08.cookingWebShopSpring.viewmodel.SearchByIngredientsData;
import com.bh08.cookingWebShopSpring.viewmodel.UserFormData;

@Controller
public class IngredSearchController {
	
	@Autowired
	private RecipeServices rdbs;
	
	@Autowired
	private SessionService sessionService;

	@RequestMapping(value = "/searchByIngr", method = RequestMethod.GET)
	public String showRecipesByIngred(Model model,
			@ModelAttribute("searchByIngredientsData") SearchByIngredientsData searchByIngredientsData) {

		if (ingredientFieldsAreNotEmpty(searchByIngredientsData)) {

			Set<Recipe> recipes = rdbs.findByIngredientsIgnoreCaseContains(searchByIngredientsData.getIngred1(),
					searchByIngredientsData.getIngred2(), searchByIngredientsData.getIngred3());

			if (recipes.isEmpty()) {
				model.addAttribute("recipes", null);
			} else {
				model.addAttribute("recipes", recipes);
			}

			model.addAttribute("userFormData", new UserFormData());
			model.addAttribute("currentUserName", sessionService.getUserName());

		} else {

			model.addAttribute("recipes", null);
			model.addAttribute("userFormData", new UserFormData());
			model.addAttribute("currentUserName", sessionService.getUserName());
		}

		return "searchByIngr.html";
	}

	private boolean ingredientFieldsAreNotEmpty(SearchByIngredientsData searchByIngredientsData) {
		return searchByIngredientsData.getIngred1() != null || searchByIngredientsData.getIngred2() != null
				|| searchByIngredientsData.getIngred3() != null;
	}
}
