package com.bh08.cookingWebShopSpring.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bh08.cookingWebShopSpring.models.Recipe;
import com.bh08.cookingWebShopSpring.models.User;
import com.bh08.cookingWebShopSpring.services.ProfileService;
import com.bh08.cookingWebShopSpring.services.RecipeServices;
import com.bh08.cookingWebShopSpring.services.SessionService;
import com.bh08.cookingWebShopSpring.viewmodel.ProfileFormData;
import com.bh08.cookingWebShopSpring.viewmodel.UserFormData;

@Controller
public class MenuController {

	@Autowired
	private SessionService sessionService;
	
	@Autowired
	private ProfileService profileService;
	
	@Autowired
	private RecipeServices rdbs;

	@RequestMapping(value = { "/", "/login", "/reg" }, method = RequestMethod.GET)
	public String loginAndReg(Model model) {
		model.addAttribute("userFormData", new UserFormData());
		model.addAttribute("currentUserName", sessionService.getUserName());
		List<Recipe> randomRecipes = rdbs.getRandomRecipes();
		model.addAttribute("recipes", randomRecipes);
		return "index.html";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(Model model, HttpSession session) {

		session.invalidate();
		model.addAttribute("userFormData", new UserFormData());
		model.addAttribute("currentUserName", sessionService.getUserName());
		List<Recipe> randomRecipes = rdbs.getRandomRecipes();
		model.addAttribute("recipes", randomRecipes);
		return "index.html";
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String loadUserData(Model model) {
		User user = profileService.loadUserData(sessionService.getUserName());
		model.addAttribute("currentUserName", sessionService.getUserName());
		model.addAttribute("profileFormData", new ProfileFormData());
		model.addAttribute("currentUserData", user);
		return "profile.html";
	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST)
	public String updateUserData(@ModelAttribute ProfileFormData profileFormData, Model model) {

		User userWithCurrentData = profileService.loadUserData(sessionService.getUserName());
		String userName = sessionService.getUserName();

		String name = profileFormData.getName();
		String phoneNumber = profileFormData.getPhoneNumber();
		String address = profileFormData.getAddress();
		String eMail = profileFormData.getEMail();

		if ("".equals(name)) {
			name = userWithCurrentData.getName();
		}
		if ("".equals(phoneNumber)) {
			phoneNumber = userWithCurrentData.getPhoneNumber();
		}
		if ("".equals(address)) {
			address = userWithCurrentData.getAddress();
		}
		if ("".equals(eMail)) {
			eMail = userWithCurrentData.getEMail();
		}

		profileService.updateUserData(userName, name, phoneNumber, address, eMail);
		User user = profileService.loadUserData(sessionService.getUserName());

		model.addAttribute("currentUserData", user);
		model.addAttribute("currentUserName", sessionService.getUserName());
		return "profile.html";
	}

	@RequestMapping(value = "/favorites", method = RequestMethod.GET)
	public String showFavorites(Model model, @RequestParam(value = "page", required = false) Integer pageNr) {
		User user = profileService.loadUserData(sessionService.getUserName());

		if (pageNr == null) {
			pageNr = 0;
		}

		Page<Recipe> recipes = rdbs.findByUserId(pageNr, user.getId());

		model.addAttribute("favoritsList", recipes);
		model.addAttribute("currentPage", pageNr);
		model.addAttribute("maxPages", recipes.getTotalPages());
		model.addAttribute("userFormData", new UserFormData());
		model.addAttribute("currentUserName", sessionService.getUserName());

		return "favorites.html";
	}

	@RequestMapping(value = "/support", method = RequestMethod.GET)
	public String showSupport(Model model) {
		model.addAttribute("userFormData", new UserFormData());
		model.addAttribute("currentUserName", sessionService.getUserName());
		return "support.html";
	}
}
