package com.bh08.cookingWebShopSpring.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bh08.cookingWebShopSpring.models.Product;
import com.bh08.cookingWebShopSpring.models.Recipe;
import com.bh08.cookingWebShopSpring.models.User;
import com.bh08.cookingWebShopSpring.services.ProfileService;
import com.bh08.cookingWebShopSpring.services.RecipeServices;
import com.bh08.cookingWebShopSpring.services.SessionService;
import com.bh08.cookingWebShopSpring.viewmodel.UserFormData;

@Controller
public class ShoppingCartController {

	@Autowired
	private SessionService sessionService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private RecipeServices rdbs;

	@RequestMapping(value = "/shoppingCart", method = RequestMethod.GET)
	public String loadProductsAndPrice(Model model, @RequestParam(value = "recipeId", required = false) Long recipeId) {

		User user = profileService.loadUserData(sessionService.getUserName());

		List<Product> products = new ArrayList<>();
		List<Integer> prices = new ArrayList<Integer>();

		Recipe recipe = rdbs.getRecipeDetails(recipeId);
		for (Product prod : recipe.getProducts()) {
			products.add(prod);
		}

		double sum = calculateSum(products);

		boolean allUserDataAdded = isAllUserDataFilledIn(user);
		model.addAttribute("allUserDataAdded", allUserDataAdded);

		model.addAttribute("userFormData", new UserFormData());
		model.addAttribute("currentUserName", sessionService.getUserName());

		model.addAttribute("recipe", recipe);
		model.addAttribute("products", products);
		model.addAttribute("sum", sum);

		return "shoppingCart.html";
	}

	private double calculateSum(List<Product> products) {
		double sum = 0;
		for (int i = 0; i < products.size(); i++) {
			sum += products.get(i).getPrice();
		}
		return sum;
	}

	private boolean isAllUserDataFilledIn(User user) {
		boolean allUserDataAdded = true;
		if (user.getName() == null || user.getPhoneNumber() == null || user.getAddress() == null
				|| user.getEMail() == null) {
			allUserDataAdded = false;
		}
		return allUserDataAdded;
	}

}
