package com.bh08.cookingWebShopSpring.viewmodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SearchFormData {

	private String searchExpression;
	private boolean submitted;
}
