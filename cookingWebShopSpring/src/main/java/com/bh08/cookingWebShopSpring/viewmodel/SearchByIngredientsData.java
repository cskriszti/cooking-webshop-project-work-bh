package com.bh08.cookingWebShopSpring.viewmodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SearchByIngredientsData {
	private String ingred1;
	private String ingred2;
	private String ingred3;

}
