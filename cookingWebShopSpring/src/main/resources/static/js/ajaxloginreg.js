$(document).ready(function() {

	$("#loginForm").submit(function(e) {
		e.preventDefault();
		if(!$(this).valid()) {
			return false;
		}
	
		var requestType = "login";
		var userName = $("#username-si").val();
		var password = $("#password-si").val();

		$.ajax({
			url : '/api/user',
			method : 'POST',
			data : JSON.stringify({
				requestType : requestType,
				userName : userName,
				password : password
			}),
			headers : {
				'Content-Type' : 'application/json',
				'Accept' : 'application/json'
			},
			success : function(data) {

				$("#login").modal("toggle");
				$(".personal").removeClass("ishidden");
				$("#regmenu").text("Welcome: " + userName);
				$("#regmenu").addClass("position");
				$(".logout").removeClass("ishidden");
				$("#loginmenu").addClass("ishidden");
				$(".buttons").removeClass("ishidden");

			},
			error : function(xhr) {
				// console.error(xhr.responseJSON.message);
				$("#loginBackendError").html(xhr.responseJSON.message);
			}
		});

	});
	

	

	// reg

	$("#regForm").submit(function(e) {
		e.preventDefault();
		if(!$(this).valid()) {
			return false;
		}
		var requestType = "reg";
		var userName = $("#username-su").val();
		var password = $("#password-su").val();
		var confirmPassword = $("#confirm-password-su").val();

		$.ajax({
			url : '/api/user',
			method : 'POST',
			data : JSON.stringify({
				requestType : requestType,
				userName : userName,
				password : password,
				confirmPassword : confirmPassword
			}),
			headers : {
				'Content-Type' : 'application/json',
				'Accept' : 'application/json'
			},
			success : function(data) {
				$("#reg").modal("toggle");
				$("#login").modal("toggle");
			},
			error : function(xhr) {
				$("#regBackendError").html(xhr.responseJSON.message);
			}
		});

	});

});