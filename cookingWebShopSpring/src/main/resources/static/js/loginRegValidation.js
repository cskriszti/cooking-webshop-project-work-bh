$(document).ready(function () {

	  $.validator.addMethod('strongPassword', function (value, element) {
		    return this.optional(element) ||
		      value.length >= 6 &&
		      /\d/.test(value);
		  }, 'PW must be at least 6 char & contain 1 num')

		  $('#loginForm').validate({
		    rules: {
		      userName: {
		        required: true,
		        minlength: 4
		      },
		      password: {
		        required: true,
		        strongPassword: true
		      }
		    }, errorPlacement: function (error, element) {
		      var errorText = error.text();
		      element.next('.error-msg').text(errorText);
		    }
		  });

		  $('#regForm').validate({
		    rules: {
		      userName: {
		        required: true,
		        minlength: 4
		      },
		      password: {
		        required: true,
		        strongPassword: true
		      },
		      confirmPassword: {
		        equalTo: "#password-su"
		      }
		    },
		    messages: {
		      confirmPassword: {
		        equalTo: "Password and confirm password don't match"

		      }
		    },
		    errorPlacement: function (error, element) {
		      var errorText = error.text();
		      element.next('.error-msg').text(errorText);
		    }

		  });

});